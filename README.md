**Input Variables**

При запуске конфигурации пользователь определяет переменную devs с type = list, содержащую всю необходимую информацию о создаваемых VM, а именно:
- prefix - назначение создаваемой VM ( lb, db, proxy, etc.)
- your_login - логин пользователя (разработчика) создающего данную инфраструктуру.

Пример задания переменной:
`["lb-username", "app1-username", "app2-username", "proxy-username"]`

Также пользователь задает имя DNS-зоны в AWS Route53.
Пример задания DNS-зоны в AWS Route53:
`devops.srwx.net`

**Usage**

Конфигурация реализована для развертывания VM в Hetzner Cloud и создания DNS записей в AWS. 
Перед запуском необходимо создать файл terraform.tfvars в директории склонированного репозитория и нем задать значения ключей как в примере:

- hcloud_token="XXXXXXXXXXXXXXXXXXXXXXX"

- aws_access_key="XXXXXXXXXXXXXXXXXXXXX"

- aws_secret_key="XXXXXXXXXXXXXXXXXXXXX"

Для запуска конфигурации находясь в дирректории необходимо выполнить:
- terraform init
- terraform apply -auto-approve

**Outputs**

В результате применения конфигурации быдует развернута инфраструктура в Hetzner Cloud.
- Имена VM - заданные при запуске конфигурации.
- Количество VM - заданные при запуске конфигурации (длина списка).
- Созданы DNS-записи для каждой VM в формате username-prefix.aws_route53_zone (которая была задана при запуске конфигурации).
- В каждой VM будет создан пользователь "terraform" со случайно сгенерированным паролем.


Информация о созданной инфраструктуре будет сохранена в локальный файл infrastructure.txt следующего содержания:
> <порядковый номер>: <DNS имя машины> <ip машины> <пароль пользователя terraform>

Пример содержания файла infrastructure.txt:

```
1: username-lb.devops.srwx.net 1.1.1.1 ungaepeSie9Thee2
2: username-app1.devops.srwx.net 2.2.2.2 iXixei9kuo3aef2i
3: username-app2.devops.srwx.net 3.3.3.3 IQuu1nahge2fae4a
4: username-proxy.devops.srwx.net 4.4.4.4 IQuu1nahge2fae4a
```






