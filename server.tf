resource "hcloud_server" "web" {
  name = "${var.devs[count.index]}"
  image       = "centos-8"
  server_type = "cx11"
  location    = "nbg1"
  count = length(var.devs)
  labels = {
    creater = "eaob"
    module = "devops"
    email = "eaob1611_at_list_ru"
    }

  provisioner "remote-exec" {
    inline = [
       "sudo adduser terraform ",
       "echo ${element(random_password.password[*].result, count.index)} | passwd terraform --stdin",   
       "echo 'AllowUsers terraform' >> /etc/ssh/sshd_config"
       ]

    connection {
      host =  self.ipv4_address
      type     = "ssh"
      user     = "root"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }
  ssh_keys    = [data.hcloud_ssh_key.eaob.id, data.hcloud_ssh_key.rebrain_ssh_key.id]
}

variable "aws_route53_zone" {
  type    = "string"
  description = <<-EOF
                 Введите имя aws_route53_zone:
                EOF
}

variable "devs" {
  type    = "list"
#  default = ["lb-eaob", "app1-eaob", "app2-eaob","proxy-eaob","db-eaob"]
  description = <<-EOF
                 Введите необходимую информацию о создаваемых VM в формате ["lb-username", "app1-username", "app2-username"], а именно:
                 prefix - назначение создаваемой VM ( lb, db, proxy, etc.)
                 your_login - логин пользователя (разработчика) создающего данную инфраструктуру.
                EOF

}

data "hcloud_ssh_key" "eaob" {
  name = "eaob_ssh_key"
}

data "hcloud_ssh_key" "rebrain_ssh_key" {
  name = "REBRAIN.SSH.PUB.KEY"
}

data "aws_route53_zone" "devops" {
  name = "${var.aws_route53_zone}"
}

resource "aws_route53_record" "web" {
  count = length(var.devs)
  zone_id = data.aws_route53_zone.devops.zone_id
  name = "${element(split("-", var.devs[count.index]),1)}-${element(split("-", var.devs[count.index]),0)}"
  type    = "A"
  ttl     = "300"
  records = ["${element(hcloud_server.web.*.ipv4_address, count.index)}"]
}

resource "random_password" "password" {
  length = 12
  count = length(var.devs)
  special = true
  override_special = "_%@"
}

resource "local_file" "infrastructure" {
  count = length(var.devs)
  content     = templatefile("${path.module}/infrastructure.tmpl",
 {
  dns    = aws_route53_record.web.*.fqdn,
  ip     = hcloud_server.web.*.ipv4_address,
  pass   = random_password.password[*].result
 }
 )               
  filename = "${path.module}/infrastructure.txt"
}
