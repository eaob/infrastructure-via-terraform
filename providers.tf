terraform {
  required_providers {

    hcloud = {
      source  = "hetznercloud/hcloud"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  required_version = ">= 0.14"
}

variable "hcloud_token" {
  type = string
  description = "Token for Hetzner Cloud"
  sensitive = true
}

provider "hcloud" {
  token = var.hcloud_token
}

variable "aws_access_key" {
  type = string
  sensitive = true
}

variable "aws_secret_key" {
  type = string
  sensitive = true
}

provider "aws" {
  region     = "us-west-2"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

